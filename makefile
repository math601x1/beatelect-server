all: stop pull build start

stop:
	pm2 stop index

start:
	pm2 start index

pull:
	git pull

build:
	npm run build

.PHONY: stop pull start all build
