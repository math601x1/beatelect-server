import { Client } from "./Client.model";
import { DataReport } from "./DataReport.model";
import { Artist } from "./SpotifyItems.model";
import { Track } from "./Track.model";

export type Opinion = "likes" | "dislikes" | "neutral"

export interface Session {
  id: string,
  sessionKey: string,
  sessionHost: Client,
  clients: Client[],
  playlistId: string,
  playlistUri: string,
  playlistTracks: Track[],
  currentPlayingTrackIndex: number,
  isCurrentlyPlaying: boolean,
  dislikedArtists: string[],
  dataReport: DataReport,
}