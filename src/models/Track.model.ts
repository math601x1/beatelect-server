export interface Track {
  id: string,
  artist: string,
  artistId: string,
  title: string,
  uri: string,
  albumUrl: string,
  hasBeenPlayed: boolean
}