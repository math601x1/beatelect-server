export interface Track {
  id: string,
  artist: string,
  title: string,
  uri: string,
  albumUrl: string,
  hasBeenPlayed: boolean,
}

export interface Artist {
  id: string,
  artistName: string,
  imageUrl: string,
}

export interface Playlist {
  id: string,
  playlistName: string,
  authorName: string,
  imageUrl: string,
}