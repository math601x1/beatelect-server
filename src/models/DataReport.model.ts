export interface DataReport {
  clients: Map<string, UserData>
  totalTracksInPlaylist: number,
  tracksPlayed: number,
  startTime: number,
  timeElapsed: string
}

export interface UserData {
  UserLikeCount: number,
  UserDislikeCount: number,
  UserAddSingleTrackCount: number,
  UserAddTracksByArtistCount: number,
  UserAddTracksByPlaylistCount: number,
  timeElapsed: number,
}