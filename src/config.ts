export const apiUrls = {
  AUTH_TOKEN_GRANT: '/api/accessTokenGrant',
  AUTH_TOKEN_SWAP: '/api/refreshTokenSwap',
}

export const authConstants = {
  CLIENT_ID: '029742eff7394f3e95b820067bdf1e66',
  CLIENT_SECRET: '7a01116a03364c5f8145d1737a73fbc5',
  REDIRECT_URI: 'https://main.d3kosm6oqdbkkr.amplifyapp.com/home',
}

export const connectionSettings = {
  corsOrigin: "*",
  port: 8443,
}
