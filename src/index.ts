import express from "express"
import cors from "cors"
import bodyParser from "body-parser"
import SpotifyWebApi from "spotify-web-api-node"
import { Server } from "socket.io"
import https from "https"
import { apiUrls, authConstants, connectionSettings } from "./config"
import { socket } from "./socket"
import fs from 'fs'
import dayjs from "dayjs"
import logger from "pino"

const log = logger({
  prettifier: true,
  base: {
    pid: false,
  },
  timestamp: () => `,"time":"${dayjs().format()}"`,
});

const options = {
  key: fs.readFileSync('/home/ubuntu/keys/privkey.pem'),
  cert: fs.readFileSync('/home/ubuntu/keys/fullchain.pem')
};


const port = connectionSettings.port
const corsOrigin = connectionSettings.corsOrigin

const app = express()
const httpsServer = https.createServer(options, app);
const io = new Server(httpsServer, {
  cors: {
    origin: corsOrigin,
    credentials: true,
  },
});

app.use(cors())
app.use(bodyParser.json())

app.post(apiUrls.AUTH_TOKEN_GRANT, async (req, res) => {
  log.info('Handling request: AuthTokenGrant')
  const code: string = req.body.code
  const spotifyApi = new SpotifyWebApi({
    redirectUri: authConstants.REDIRECT_URI,
    clientId: authConstants.CLIENT_ID,
    clientSecret: authConstants.CLIENT_SECRET,
  })

  spotifyApi
    .authorizationCodeGrant(code)
    .then(data => {
      res.json({
        accessToken: data.body.access_token,
        refreshToken: data.body.refresh_token,
        expiresIn: data.body.expires_in,
      })
    })
    .catch(err => {
      log.info(err)
      res.status(400).send(err)
    })
})

app.post(apiUrls.AUTH_TOKEN_SWAP, (req, res) => {
  log.info('Handling request: AuthTokenSwap')
  const refreshToken = req.body.refreshToken
  const spotifyApi = new SpotifyWebApi({
    redirectUri: authConstants.REDIRECT_URI,
    clientId: authConstants.CLIENT_ID,
    clientSecret: authConstants.CLIENT_SECRET,
    refreshToken,
  })

  spotifyApi
    .refreshAccessToken()
    .then(data => {
      res.json({
        accessToken: data.body.access_token,
        expiresIn: data.body.expires_in,
      })
    })
    .catch(err => {
      log.info(err)
      res.sendStatus(400)
    })
})

app.get("/test", (req, res) => {
  res.sendStatus(200)
})

httpsServer.listen(port, () => {
  log.info(`Server started listening at * on port ${port}.`)

  socket({ io })
})

