import { Server } from "socket.io"
import { Session } from "./models/Session.model"
import { Client } from "./models/Client.model"
import { Track } from "./models/Track.model"
import { DataReport, UserData } from "./models/DataReport.model"
import logger from "pino"
import dayjs from "dayjs"
import { nanoid } from "nanoid"

const log = logger({
  prettifier: true,
  base: {
    pid: false,
  },
  timestamp: () => `,"time":"${dayjs().format()}"`,
});

const stringNumbers = (length: number) => {
  let result = '';
  const characters = '0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() *
      charactersLength));
  }
  return result;
}

export const SOCKET_EVENTS = {
  CONNECT: "connect",
  DISCONNECT: "disconnect",
  CLIENT: {
    SHARE_DETAILS: "share_details",
    CREATE_SESSION: "create_session",
    JOIN_SESSION: "join_session",
    ADD_TRACKS_TO_PLAYLIST: "add_tracks_to_playlist",
    HAS_ADDED_TRACKS_TO_PLAYLIST: "has_added_tracks_to_playlist",
    UPDATE_CURRENTLY_PLAYING_TRACK: "client_update_currently_playing_track",
    NEW_TRACK_RECOMMENDATIONS: "new_track_recommendations",
    UPDATE_PLAYBACK_STATE: "client_update_playback_state",
    GET_PLAYBACK_STATE: "get_playback_state",
    LIKE_TRACK: "like_track",
    HAS_LIKED_TRACK: "has_liked_track",
    DISLIKE_TRACK: "dislike_track",
  },
  SERVER: {
    UPDATE_SESSIONS: "update_sessions",
    UPDATE_SESSION: "update_session",
    UPDATE_PLAYBACK_STATE: "server_update_playback_state",
    REMOVE_ARTIST_TRACKS: "server_remove_artist_tracks",
    CLIENT_DISCONNECT: "server_client_disconnect",
    ADD_TRACKS_TO_PLAYLIST: "server_add_tracks_to_playlist",
    LIKE_TRACK: "server_like_track",
  }
}

const clients: Client[] = []
const sessions: Session[] = []

const userData = {
  UserAddSingleTrackCount: 0,
  UserAddTracksByArtistCount: 0,
  UserAddTracksByPlaylistCount: 0,
  UserDislikeCount: 0,
  UserLikeCount: 0,
} as UserData

const getClient = (socketId: string) => {
  return clients.find(client => client.socketId === socketId)
}

const getCurrentSession = (socketId: string) => {
  return sessions.find(session => session.clients.some(client => client.socketId === socketId))
}

export const socket = ({ io }: { io: Server }) => {

  io.on(SOCKET_EVENTS.CONNECT, s => {
    log.info(`Client connected to server: ${s.id}`)

    s.broadcast.emit(SOCKET_EVENTS.SERVER.UPDATE_SESSIONS, sessions)
    s.emit(SOCKET_EVENTS.SERVER.UPDATE_SESSIONS, sessions)

    s.on(SOCKET_EVENTS.CLIENT.SHARE_DETAILS, (newClient: Client) => {
      clients.push(newClient)
    })

    s.on(SOCKET_EVENTS.CLIENT.CREATE_SESSION, (playlistId: string, playlistUri: string) => {
      const newSessionHost = getClient(s.id)

      const session = {
        id: nanoid(),
        sessionKey: stringNumbers(4),
        sessionHost: newSessionHost,
        clients: [newSessionHost],
        playlistId,
        playlistUri,
        playlistTracks: [] as Track[],
        visiblePlaylistTracks: [] as Track[],
        currentPlayingTrackIndex: 0,
        isCurrentlyPlaying: false,
        dislikedArtists: [],
        dataReport: {
          clients: new Map<string, UserData>(),
          totalTracksInPlaylist: 0,
          tracksPlayed: 0,
          startTime: new Date().getTime(),
          timeElapsed: ""
        } as DataReport,
      } as Session

      sessions.push(session)
      session.dataReport.clients.set(s.id, userData as UserData)

      s.join(session.id)
      io.in(session.id).emit(SOCKET_EVENTS.SERVER.UPDATE_SESSION, session)

      log.info(`Client has created a new session with id: ${session.id}`)
    })

    s.on(SOCKET_EVENTS.CLIENT.JOIN_SESSION, (sessionId: string) => {
      const sessionToJoin = sessions.find(session => session.id === sessionId)
      const clientToJoin = clients.find(client => client.socketId === s.id)
      sessionToJoin.clients.push(clientToJoin)

      s.join(sessionToJoin.id)
      io.in(sessionToJoin.id).emit(SOCKET_EVENTS.SERVER.UPDATE_SESSION, sessionToJoin)
      log.info(`Client has joined session with id: ${sessionToJoin.id}`)

      sessionToJoin.dataReport.clients.set(s.id, userData as UserData)
    })

    s.on(SOCKET_EVENTS.DISCONNECT, () => {
      const disconnectedClient = getClient(s.id)
      const sessionToRemoveClient = getCurrentSession(s.id)

      if (disconnectedClient) {
        const index = clients.findIndex(client => client.socketId === s.id);
        if (index !== -1) clients.splice(index, 1)
      }

      if (sessionToRemoveClient) {
        const index = sessionToRemoveClient.clients.findIndex(client => client.socketId === s.id)
        if (index !== -1) sessionToRemoveClient.clients.splice(index, 1)
      }

      sessionToRemoveClient.dataReport.totalTracksInPlaylist = sessionToRemoveClient.playlistTracks.length
      sessionToRemoveClient.dataReport.tracksPlayed = sessionToRemoveClient.playlistTracks.filter(track => track.hasBeenPlayed === true).length
      sessionToRemoveClient.dataReport.timeElapsed = ((new Date().getTime() - sessionToRemoveClient.dataReport.startTime) / 1000 / 60).toString().concat(" minutes")

      const json = JSON.stringify(Object.assign(sessionToRemoveClient.dataReport, Object.fromEntries(sessionToRemoveClient.dataReport.clients)), null, 2);

      io.in(sessionToRemoveClient.id).emit(SOCKET_EVENTS.SERVER.CLIENT_DISCONNECT, json)

      io.in(sessionToRemoveClient.id).emit(SOCKET_EVENTS.SERVER.UPDATE_SESSION, sessionToRemoveClient)
      log.info(`Client has disconnected from server: ${s.id}`)

      if (sessionToRemoveClient?.clients.length === 0) {
        const index = sessions.findIndex(session => session.id === sessionToRemoveClient.id)
        if (index !== -1) sessions.splice(index, 1)
        log.info(`Session has been terminated: ${sessionToRemoveClient.id}: No clients present`)
      }
    })

    s.on(SOCKET_EVENTS.CLIENT.ADD_TRACKS_TO_PLAYLIST, (tracks: Track[]) => {
      const sessionToAddTracks = getCurrentSession(s.id)
      io.to(sessionToAddTracks.sessionHost.socketId).emit(SOCKET_EVENTS.SERVER.ADD_TRACKS_TO_PLAYLIST, tracks)
    })

    s.on(SOCKET_EVENTS.CLIENT.HAS_ADDED_TRACKS_TO_PLAYLIST, (tracks: Track[]) => {
      const sessionToAddTracks = getCurrentSession(s.id)
      sessionToAddTracks.playlistTracks = sessionToAddTracks.playlistTracks?.concat(tracks)
      io.in(sessionToAddTracks.id).emit(SOCKET_EVENTS.SERVER.UPDATE_SESSION, sessionToAddTracks)

      const currentClientData = sessionToAddTracks.dataReport.clients.get(s.id)
      if (tracks.length === 1) {
        sessionToAddTracks.dataReport.clients.set(s.id, {
          ...currentClientData,
          UserAddSingleTrackCount: currentClientData.UserAddSingleTrackCount + 1
        })
      } else if (tracks.length > 1 && tracks.length < 15) {
        sessionToAddTracks.dataReport.clients.set(s.id, {
          ...currentClientData,
          UserAddTracksByArtistCount: currentClientData.UserAddTracksByArtistCount + 1
        })
      } else {
        sessionToAddTracks.dataReport.clients.set(s.id, {
          ...currentClientData,
          UserAddTracksByPlaylistCount: currentClientData.UserAddTracksByPlaylistCount + 1
        })
      }
    })

    s.on(SOCKET_EVENTS.CLIENT.UPDATE_CURRENTLY_PLAYING_TRACK, (currentlyPlayingTrackId: string) => {
      const sessionToUpdateCurrentlyPlayingTrack = getCurrentSession(s.id)
      let currentPlayingTrackIndex: number

      currentPlayingTrackIndex = sessionToUpdateCurrentlyPlayingTrack.playlistTracks?.findIndex(track => track.id === currentlyPlayingTrackId && track.hasBeenPlayed === false)
      if (currentPlayingTrackIndex !== -1) {
        sessionToUpdateCurrentlyPlayingTrack.playlistTracks[currentPlayingTrackIndex] = {
          ...sessionToUpdateCurrentlyPlayingTrack.playlistTracks[currentPlayingTrackIndex],
          hasBeenPlayed: true
        } as Track
      } else {
        const trackIndices: number[] = []
        sessionToUpdateCurrentlyPlayingTrack.playlistTracks?.forEach((track, index) => {
          if (track.id === currentlyPlayingTrackId) {
            trackIndices.push(index)
          }
        })
        currentPlayingTrackIndex = Math.max(...trackIndices)
        sessionToUpdateCurrentlyPlayingTrack.playlistTracks[currentPlayingTrackIndex + 1] = {
          ...sessionToUpdateCurrentlyPlayingTrack.playlistTracks[currentPlayingTrackIndex + 1],
          hasBeenPlayed: false
        } as Track
      }

      sessionToUpdateCurrentlyPlayingTrack.currentPlayingTrackIndex = currentPlayingTrackIndex
      io.in(sessionToUpdateCurrentlyPlayingTrack.id).emit(SOCKET_EVENTS.SERVER.UPDATE_SESSION, sessionToUpdateCurrentlyPlayingTrack)
      log.info(`Currently Playing track is ${sessionToUpdateCurrentlyPlayingTrack.playlistTracks[currentPlayingTrackIndex]?.title}`)
    })

    s.on(SOCKET_EVENTS.CLIENT.NEW_TRACK_RECOMMENDATIONS, (recommendedTracks: Track[]) => {
      const currentSession = getCurrentSession(s.id)
      currentSession.playlistTracks = currentSession.playlistTracks?.concat(recommendedTracks)

      io.in(currentSession.id).emit(SOCKET_EVENTS.SERVER.UPDATE_SESSION, currentSession)
    })

    s.on(SOCKET_EVENTS.CLIENT.UPDATE_PLAYBACK_STATE, (isPlaying: boolean) => {
      const currentSession = getCurrentSession(s.id)
      currentSession.isCurrentlyPlaying = isPlaying
      io.in(currentSession.id).emit(SOCKET_EVENTS.SERVER.UPDATE_PLAYBACK_STATE, currentSession.isCurrentlyPlaying)
    })

    s.on(SOCKET_EVENTS.CLIENT.GET_PLAYBACK_STATE, () => {
      const currentSession = getCurrentSession(s.id)
      io.in(currentSession.id).emit(SOCKET_EVENTS.SERVER.UPDATE_PLAYBACK_STATE, currentSession.isCurrentlyPlaying)
    })

    s.on(SOCKET_EVENTS.CLIENT.LIKE_TRACK, (trackId: string) => {
      const currentSession = getCurrentSession(s.id)
      io.to(currentSession.sessionHost.socketId).emit(SOCKET_EVENTS.SERVER.LIKE_TRACK, trackId)
    })

    s.on(SOCKET_EVENTS.CLIENT.HAS_LIKED_TRACK, (likedTracks: Track[], selectedTrackPosition: number, isAlternating: boolean) => {
      const currentSession = getCurrentSession(s.id)
      if (isAlternating) {
        const positions = [] as number[]
        for (let i = 1; i < 4; i++) {
          positions.push(selectedTrackPosition + (i * 2) - 1)
        }
        likedTracks.forEach(track => currentSession.playlistTracks.splice(positions.shift(), 0, track))
      } else {
        currentSession.playlistTracks = currentSession.playlistTracks?.concat(likedTracks)
      }

      io.in(currentSession.id).emit(SOCKET_EVENTS.SERVER.UPDATE_SESSION, currentSession)

      const currentData = currentSession.dataReport.clients.get(s.id)
      currentSession.dataReport.clients.set(s.id, {
        ...currentData,
        UserLikeCount: currentData.UserLikeCount + 1
      })
    })

    s.on(SOCKET_EVENTS.CLIENT.DISLIKE_TRACK, (dislikedTrack: Track, selectedTrackPosition: number) => {
      log.info("client disliked track at " + selectedTrackPosition)
      const currentSession = getCurrentSession(s.id)

      if ((selectedTrackPosition !== currentSession.currentPlayingTrackIndex) && (currentSession?.dislikedArtists.some(artistId => artistId === dislikedTrack.artistId) === false)) {
        currentSession.playlistTracks.splice(selectedTrackPosition, 1)
        io.to(currentSession.sessionHost.socketId).emit(SOCKET_EVENTS.SERVER.REMOVE_ARTIST_TRACKS, [selectedTrackPosition])
      }

      if (currentSession.dislikedArtists.some(artistId => artistId === dislikedTrack.artistId)) {
        const artistTracks = currentSession.playlistTracks.filter(track => track.artistId === dislikedTrack.artistId)

        const trackIndices: number[] = []
        artistTracks.forEach(track => {
          const index = currentSession.playlistTracks.findIndex(t => t.id === track.id && t.hasBeenPlayed === false)
          if (index !== -1) trackIndices.push(index)
        })

        if (trackIndices.length > 0) {
          let offSet = 0 as number
          trackIndices.forEach(index => {
            log.info("removing track at " + index)
            currentSession.playlistTracks.splice(index - offSet, 1)
            offSet = offSet + 1
          })
          io.to(currentSession.sessionHost.socketId).emit(SOCKET_EVENTS.SERVER.REMOVE_ARTIST_TRACKS, trackIndices)
        }
      }

      if (!currentSession.dislikedArtists.some(artistId => artistId === dislikedTrack.artistId)) {
        log.info("adding to disliked artists: " + dislikedTrack.artistId)
        currentSession.dislikedArtists.push(dislikedTrack.artistId)
      }

      io.in(currentSession.id).emit(SOCKET_EVENTS.SERVER.UPDATE_SESSION, currentSession)

      const currentData = currentSession.dataReport.clients.get(s.id)
      currentSession.dataReport.clients.set(s.id, {
        ...currentData,
        UserDislikeCount: currentData.UserDislikeCount + 1
      })
    })
  })
}